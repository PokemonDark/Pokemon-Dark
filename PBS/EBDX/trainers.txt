#-------------------------------
[LEADER_Brock]
TrainerPositionX = -8
BattleEnv        = STAGE
BattleVS         = classicVS
#-------------------------------
[LEADER_Brock,Brock]
BattleScript     = BROCK
#-------------------------------
[TEAMROCKET_M]
BattleIntro      = evilTeam
BattleVS         = trainerSM
#-------------------------------
[TEAMROCKET_F]
BattleIntro      = evilTeam
BattleVS         = trainerSM
#-------------------------------
[SAGE1]
BattleIntro      = evilTeam
BattleVS         = spaceSM
BattleScript     = AQUA
#-------------------------------
[ECLIPSE]
BattleIntro      = evilTeam
BattleVS         = spaceSM
#-------------------------------
[RIVAL1,Michael]
BattleIntro      = rainbowIntro
BattleVS         = specialSM
BattleScript     = MICHAELCHARMANDER
Ace = 2
#-------------------------------
[RIVAL1,Michael,1]
BattleIntro      = rainbowIntro
BattleVS         = specialSM
BattleScript     = MICHAELSQUIRTLE
Ace = 2
#-------------------------------
[RIVAL1,Michael,2]
BattleIntro      = rainbowIntro
BattleVS         = specialSM
BattleScript     = MICHAELBULBASAUR
Ace = 2
#-------------------------------
[RIVAL1,Michael,3]
BattleIntro      = rainbowIntro
BattleVS         = specialSM
BattleScript     = MICHAELIVYSAUR
Ace = 2
#-------------------------------
[RIVAL1,Michael,4]
BattleIntro      = rainbowIntro
BattleVS         = specialSM
BattleScript     = MICHAELCHARMELEON
Ace = 2
#-------------------------------
[RIVAL1,Michael,5]
BattleIntro      = rainbowIntro
BattleVS         = specialSM
BattleScript     = MICHAELWARTORTLE
Ace = 2
#-------------------------------
[RIVAL_F,Lisa]
BattleVS         = specialSM
#-------------------------------
[RIVAL_M,Gerard]
BattleVS         = specialSM
#-------------------------------
[LEADER_Rose,Rose]
BattleIntro = Elite
BattleVS         = specialSM
BattleScript = ROSE
Ace = 3
#-------------------------------
[LEADER_Anu,Anu]
BattleIntro = Elite
BattleVS         = specialSM
Ace = 4
#-------------------------------
[BUGCATCHER,Adam]
BattleScript = TESTSTRING