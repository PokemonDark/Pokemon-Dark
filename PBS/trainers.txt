﻿

[YOUNGSTER,Liam]
LoseText = "Aww, I lost."
Pokemon = DEERLING,3

[BACKPACKER_M,Joe]
LoseText = "Time to head back home."
Pokemon = MILTANK, 4

[BUGCATCHER,Stephen]
LoseText = "Man! You squished my bugs.."
Pokemon = WEEDLE,4

[BUGCATCHER,Adam]
LoseText = "Aw.. I've waited a whole day for this Grotto!"
Pokemon = METAPOD,10
Pokemon = METAPOD,10
Pokemon = METAPOD,10
Pokemon = METAPOD,10

[AROMALADY,Ciara]
LoseText = "I'm going to go for a walk.."
Pokemon = HATENNA,5
Pokemon = PIKACHU,7

[RIVAL1,Michael]
LoseText = "You can battle this good already?!"
Pokemon = PIDGEY,3
Pokemon = TEPIG,6

[RIVAL1,Michael,1]
LoseText = "You can battle this good already?!"
Pokemon = PIDGEY,3
Pokemon = FROAKIE,6

[RIVAL1,Michael,2]
LoseText = "You can battle this good already?!"
Pokemon = PIDGEY,3
Pokemon = TURTWIG,6

[ECLIPSE,Druid]
LoseText = "Entropy is inevitable."
Pokemon = SABLEYE,10

[ECLIPSE,Druid,1]
LoseText = "Listen to the void. Can you hear it?"
Pokemon = SPINARAK,8

[ECLIPSE,Druid,2]
LoseText = "Let loose your shackles to this World."
Pokemon = HOUNDOUR,8

[ECLIPSE,Druid,3]
LoseText = "We have come to forge our new world."
Pokemon = MURKROW,8

[SAGE1,Aqua]
LoseText = "Hmm.."
Pokemon = HOUNDOUR,8
Pokemon = SABLEYE,9
Pokemon = ONIX,11
    Form = 3


[TUBER_M,Charles]
LoseText = "You stood on my sandcastle!"
Pokemon = SANDYGAST,10

[TUBER_F,???]
LoseText = "..."
Pokemon = GASTLY,12
Pokemon = HAUNTER,14

[BIKER,Odhran]
LoseText = "I feel sick now too..."
Pokemon = GRIMER,9

[SCHOOLKID_M,Patrick]
LoseText = "I should have focused on my own exam..."
Pokemon = GEODUDE,13
Pokemon = GEODUDE,13

[SCHOOLKID_M,Patrick,1]
LoseText = "I should have focused on my own exam..."
Pokemon = NOSEPASS,13

[PICNICKER,Emma]
LoseText = "This exam is too difficult! No fair!"
Pokemon = ROGGENROLA,13
Pokemon = ROGGENROLA,13

[PICNICKER,Emma,1]
LoseText = "This exam is too difficult! No fair!"
Pokemon = RHYHORN,14

[LASS2,Fionnuala]
LoseText = "You deserve to challenge for this badge, good luck!"
Pokemon = GEODUDE,13
Pokemon = ROLYCOLY,13

[LASS2,Fionnuala,1]
LoseText = "You deserve to challenge for this badge, good luck!"
Pokemon = ROGGENROLA,13
Pokemon = ARON,13

[LEADER_Rose,Rose]
Items = SUPERPOTION
LoseText = "Ah. Your potential is amazing!"
Pokemon = NOSEPASS,15
Pokemon = ARON,15
Pokemon = ONIX,18

[SAGE1,Shadow]
LoseText = "Hmm.."
Pokemon = SABLEYE,6
Pokemon = MEWTWO,20
    Form = 3
Pokemon = ONIX,20
    Form = 3

[CHAMPION,Blue]
Pokemon = CALYREX,10

[LASS,Crissy]
Pokemon = TURTWIG,10

[RIVAL_F,Lisa]
LoseText = "You can battle this good already?!"
Pokemon = CHARMANDER,6

[RIVAL_M,Gerard]
LoseText = "Huh? What a battle."
Pokemon = CYNDAQUIL,6

[INTERVIEWERS,"Donna and Julian"]
LoseText = "Amazing Footage!
Pokemon = PIKACHU,10
Pokemon = EEVEE, 10

[HIKER,Clifford]
LoseText = "Hopefully I can find my way out!"
Pokemon = NOSEPASS,15
Pokemon = ARON,10

[FISHERMAN,Fintin]
LoseText = "I took the bait!"
Pokemon = CHEWTLE,10
Pokemon = MAGIKARP,10

[COOLTRAINER_F,"Maria"]
LoseText = "I would be no match for it anyway..."
Pokemon = ZUBAT,12
Pokemon = ZUBAT,12
Pokemon = ONIX,14

[BACKPACKER_F,"Kathryn"]
LoseText = "I just want a Rhyperior!"
Pokemon = ROLYCOLY,10
Pokemon = ROLYCOLY,10
Pokemon = ARON,13

[RIVAL1,Michael,3]
LoseText = "What a battle! The Gym challenge is already making us better trainers!"
Pokemon = PIDGEY,14
Pokemon = GROTLE,16

[RIVAL1,Michael,4]
LoseText = "What a battle! The Gym challenge is already making us better trainers!"
Pokemon = PIDGEY,14
Pokemon = PIGNITE,16

[RIVAL1,Michael,5]
LoseText = "What a battle! The Gym challenge is already making us better trainers!"
Pokemon = PIDGEY,14
Pokemon = FROGADIER,16

[CAMPER,Tommy]
LoseText = "Ah. That explains it, you aren't a Pokémon!"
Pokemon = SWABLU,16
Pokemon = DUCKLETT,17

[BLACKBELT,Luke]
LoseText = "Need to do some more training."
Pokemon = ZANGOOSE,17
Pokemon = SEVIPER,17

[HIKER,Brandon]
LoseText = "You have surpassed my greatest hike!"
Pokemon = RHYHORN,17

[COWGIRL,Meabh]
LoseText = "One step closer!"
Pokemon = BUDEW,16
Pokemon = MILTANK,17

[LADY,Deirdre]
LoseText = "Your Pokémon are pretty wonderful as well!"
Pokemon = ROSELIA,16
Pokemon = AUDINO,17

[SCHOOLKID_M,Jamie]
LoseText = "I should learn from watching other battles."
Pokemon = GOOMY,18

[BLACKBELT2,Conor]
LoseText = "Once I get a Lucario, I'll be unstoppable."
Pokemon = RIOLU,23

[SUPERNERD,Callum]
LoseText = "4 years and I'm still terrible! Need to work on my ELO."
Pokemon = CRANIDOS,22
Pokemon = BAGON, 20

[SCIENTIST_F,Fia]
LoseText = "More research is needed."
Pokemon = DRUDDIGON,22

[WORKER2,"Pat"]
LoseText = "I wish I didn't have to work so much."
Pokemon = ONIX,21
Pokemon = WOOBAT, 22

[ECLIPSE,Druid,4]
LoseText = "Not again!"
Pokemon = MURKROW,22
Pokemon = TORKOAL, 22

[ECLIPSE,Druid,5]
LoseText = "We will always get back up again."
Pokemon = ZORUA,22
Pokemon = ARIADOS, 22

[ECLIPSE,Druid,6]
LoseText = "You're cursed!"
Pokemon = MURKROW,22
Pokemon = SABLEYE, 21

[FIREBREATHER,"Simon"]
LoseText = "But you scare me!"
Pokemon = CARKOL,22

[BACKPACKER_F,"Kim"]
LoseText = "I just want a Rhyperior!"
Pokemon = TORKOAL,21
Pokemon = SLUGMA,21

[HARLEQUIN,"Magstar"]
LoseText = "Magmar is the best, how can this happen?!"
Pokemon = MAGBY,21
Pokemon = MAGMAR,23

[LASS2,"Aoife"]
LoseText = "I can see clearly now!"
Pokemon = SWABLU,23
Pokemon = LOMBRE,23

[SAILOR,"Gary"]
LoseText = "You Northies know your Pokémon!"
Pokemon = PSYDUCK,23
Pokemon = PELIPPER,23
Pokemon = PELIPPER,23

[POKEMONBREEDER_F,"Lucie"]
LoseText = "Your Pokémon are amazing!"
Pokemon = BAGON,22
Pokemon = DEWPIDER,23

[LEADER_Anu,"Anu"]
Items = SUPERPOTION
LoseText = "The fates have willed it."
Pokemon = LUDICOLO,25
Pokemon = PELIPPER,25
Pokemon = GOLDUCK,25
Pokemon = GYARADOS,30
