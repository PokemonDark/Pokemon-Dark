module QuestModule
  
  # You don't actually need to add any information, but the respective fields in the UI will be blank or "???"
  # I included this here mostly as an example of what not to do, but also to show it's a thing that exists
  Quest0 = {
  
  }
  

#GQ
  Quest1 = {
    :ID => "1",
    :Name => "This must be a bad dream...",
    :QuestGiver => "Professor Alder",
    :Stage1 => "Talk to Professor Alder outside of Tamarach Forest.",
    :Stage2 => "Regroup with the Professor in Tamarach Forest.",
    :Stage3 => "Find out more about what happened to your Dad at the Pokémon Lab in Tamarach Forest.",
    :Stage4 => "Head to Carryport Town to confront the hooded figures.",
    :Stage5 =>  "Pick up the Eclipse Energy Orb in Carryport Town.",
    :Stage6 => "Find the missing portal charges in Carryport Town.",
    :Stage7 => "Activate the portal in Carryport Town.",
    :Stage8 => "Tell your Mum what happened at Carryport Town.",
    :QuestDescription => "Investigate what has happened to your Father.",
    :RewardString => "nil"
  }

  Quest2 = {
    :ID => "2",
    :Name => "Modern Superheroes!",
    :QuestGiver => "Young Boy",
    :Stage1 => "Get an autograph from a Pokémon Researcher at the Pokémon Lab.",
    :Stage2 => "Return the autograph to the boy in Flora Town.",
    :Location1 => "Pokémon Lab",
    :Location2 => "Flora Town",
    :QuestDescription => "Michael's brother suggested that Amarch Hostel gives out a Town Map.",
    :RewardString => "Town Map"
  }

  Quest3 = {
    :ID => "3",
    :Name => "Not that way!",
    :QuestGiver => "Michael's Brother",
    :Stage1 => "Get a Town Map at Amarch Hostel.",
    :Location1 => "Amarch Hostel",
    :QuestDescription => "Michael's brother suggested that Amarch Hostel gives out a Town Map.",
    :RewardString => "Town Map"
  }

  Quest4 = {
    :ID => "4",
    :Name => "I wanna be the very best!",
    :QuestGiver => "Professor Alder",
    :Stage1 => "Beat the first Pokémon Gym in Shellside Town.",
    :Stage2 => "Beat the second Pokémon Gym in Cethlenn City.",
    :Stage3 => "Beat the third Pokémon Gym in Cethlenn City.",
    :Stage4 => "Beat the fourth Pokémon Gym in Cethlenn City.",
    :Stage5 => "Beat the fifth Pokémon Gym in Cethlenn City.",
    :Stage6 => "Beat the sixth Pokémon Gym in Cethlenn City.",
    :Stage7 => "Beat the seventh Pokémon Gym in Cethlenn City.",
    :Stage8 => "Beat the eighth Pokémon Gym in Cethlenn City.",
    :Stage9 => "Become Champion of the Eiras Region.",
    :Location1 => "Shellside Town",
    :Location2 => "Cethlenn City",
    :Location3 => "Carryport Town",
    :Location4 => "Carryport Town",
    :Location5 => "Carryport Town",
    :Location6 => "Carryport Town",
    :Location7 => "Carryport Town",
    :Location8 => "Carryport Town",
    :Location9 => "Carryport Town",
    :QuestDescription => "Travel around the Eiras Region and become Champion.",
    :RewardString => "Hall of Fame"
  }

  Quest5 = {
    :ID => "5",
    :Name => "Heavy is the head.",
    :QuestGiver => "Explorer",
    :Stage1 => "Find five strange key fragments around the Eiras Region.",
    :Stage2 => "Find the keyhole in Tamarach Forest.",
    :Location1 => "Eiras",
    :Location2 => "Tamarach Forest",
    :QuestDescription => "Unlock a secret in Tamarach Forest.",
    :RewardString => "???"
  }

  Quest6 = {
    :ID => "6",
    :Name => "Leaving Home.",
    :QuestGiver => "Professor Alder",
    :Stage1 => "Speak to your Mum in Flora Town.",
    :Stage2 => "Board the S.S. Finn in Carryport Town.",
    :Location1 => "Flora Town",
    :QuestDescription => "Begin your adventure in the Eiras Region.",
    :RewardString => "nil"
  }

  Quest7 = {
    :ID => "7",
    :Name => "Hole in the Hedge.",
    :QuestGiver => "Grotto Hunter",
    :Stage1 => "Find a Hidden Grotto.",
    :Stage2 => "Find all the Hidden Grottos in Eiras.",
    :QuestDescription => "Hidden Grottos contain unique Pokémon and items, find them all!",
    :RewardString => "???"
  }


  Quest8 = {
    :ID => "8",
    :Name => "Spelunking!",
    :QuestGiver => "Clumsy Hiker",
    :Stage1 => "Find the Hikers Lantern in Cave Cloch.",
    :Location1 => "Cave Cloch",
    :QuestDescription => "A Hiker has lost his Lantern in Cave Cloch, he says we can keep it if we find it!",
    :RewardString => "Lantern"
  }

  Quest9 = {
    :ID => "9",
    :Name => "Reeling in the years!",
    :QuestGiver => "Fisherman Brother #2",
    :Stage1 => "Catch a Magikarp and return to the Fisherman.",
    :QuestDescription => "A generous Fisherman gave us a Old Rod, he wants us to catch a Magikarp and show it to him. It's the least we can do!",
    :RewardString => "Mystic Water"
  }

  Quest10 = {
    :ID => "10",
    :Name => "Ghostbusters!",
    :Stage1 => "Investigate Apartment 2 in Shellside Town.",
    :Location1 => "Shellside Town",
    :QuestDescription => "This place gives me the creeps.",
    :RewardString => "???"
  }

  Quest11 = {
    :ID => "11",
    :Name => "Cave Disturbances!",
    :Stage1 => "Investigate Marmair Cave.",
    :Location1 => "Marmair Cave",
    :QuestDescription => "There are reports Eclipse are up to something in Marmair Cave.",
  }

  Quest12 = {
    :ID => "12",
    :Name => "Not tonight.",
    :Stage1 => "Find a way into the PokéPub.",
    :Location1 => "Cethlenn City",
    :Stage2 => "Use the Dodgy Trainer ID to get into the PokéPub.",
    :Location2 => "Cethlenn City",
    :QuestDescription => "I need to find a way in to the PokéPub in Cethlenn City",
  }

  Quest13 = {
    :ID => "13",
    :Name => "We Will Rock You!",
    :Stage1 => "Hire a the first band member.",
    :Location1 => "Cethlenn City",
    :Stage2 => "Hire a the second band member.",
    :Location2 => "Cethlenn City",
    :Stage3 => "Hire a the third band member.",
    :Location3 => "Cethlenn City",
    :QuestDescription => "I need to find band members for the Cethlenn City PokéPub.",
  }


  Quest14 = {
    :ID => "14",
    :Name => "Castle Crashers!",
    :Stage1 => "Find a way into the artifact.",
    :Location1 => "Cethlenn City",
    :Stage1 => "Return the artifact.",
    :Location1 => "Cethlenn City",
    :QuestDescription => "Find an artifact from the now destroyed Cethlenn Castle using the ItemFinder.",
  }

end
