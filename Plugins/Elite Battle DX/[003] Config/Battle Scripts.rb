#===============================================================================
#  Elite Battle: DX
#    by Luka S.J.
# ----------------
#  Battle Scripts
#===============================================================================
module BattleScripts
  # example scripted battle for PIDGEY
  # you can define other scripted battles in here or make your own section
  # with the BattleScripts module for better organization as to not clutter the
  # main EBDX cofiguration script (or keep it here if you want to, your call)
  PIDGEY = {
    "turnStart0" => {
      :text => [
        "Wow! This here Pidgey is among the top percentage of Pidgey.",
        "I have never seen such a strong Pidgey!",
        "Btw, this feature works even during wild battles.",
        "Pretty exciting, right?"
      ],
      :file => "trainer024"
    }
  }
  # to call this battle script run the script from an event jusst before the
  # desired battle:
  #    EliteBattle.set(:nextBattleScript, :PIDGEY)
  #-----------------------------------------------------------------------------
  # example scripted battle for BROCK
  # this one is added to the EBDX trainers PBS as a BattleScript parameter
  # for the specific battle of LEADER_Brock "Brock" trainer
  BROCK = {
    "turnStart0" => proc do
      pname = @battlers[1].name
      tname = @battle.opponent[0].name
      # begin code block for the first turn
      @scene.pbTrainerSpeak(["Time to set this battle into motion!",
                             "Let's see if you'll be able to handle my #{pname} after I give him this this!"
                           ])
      # play common animation for Item use args(anim_name, scene, index)
      @scene.pbDisplay("#{tname} tossed an item to the #{pname} ...")
      EliteBattle.playCommonAnimation(:USEITEM, @scene, 1)
      # play aura flare
      @scene.pbDisplay("Immense energy is swelling up in the #{pname}")
      EliteBattle.playCommonAnimation(:AURAFLARE, @scene, 1)
      @vector.reset # AURAFLARE doesn't reset the vector by default
      @scene.wait(16, true) # set true to anchor the sprites to vector
      # raise battler Attack sharply (doesn't display text)
      @battlers[1].pbRaiseStatStageBasic(:ATTACK, 2)
      # show trainer speaking additional text
      @scene.pbTrainerSpeak("My #{pname} will not falter!")
      # show generic text
      @scene.pbDisplay("The battle is getting intense! You see the lights and stage around you shift.")
      # change Battle Environment (with white fade)
      pbBGMPlay("Battle Elite")
      @sprites["battlebg"].reconfigure(:STAGE, Color.white)
    end,
    "damageOpp" => "Woah! A powerful move!",
    "damageOpp2" => "Another powerful move ...",
    "lastOpp" => "This is it! Let's make it count!",
    "lowHPOpp" => "Hang in there!",
    "attack" => "Whatever you throw at me, my team can take it!",
    "attackOpp" => "How about you try this one on for size!",
    "fainted" => "That's how we do it in this gym!",
    "faintedOpp" => "Arghh. You did well my friend...",
    "loss" => "You can come back and challenge me any time you want."
  }
  #-----------------------------------------------------------------------------
  # example Dialga fight
  DIALGA = {
    "turnStart0" => proc do
      # hide databoxes
      @scene.pbHideAllDataboxes
      # show flavor text
      @scene.pbDisplay("The ruler of time itself; Dialga starts to radiate tremendous amounts of energy!")
      @scene.pbDisplay("Something is about to happen ...")
      # play common animation
      EliteBattle.playCommonAnimation(:ROAR, @scene, 1)
      @scene.pbDisplay("Dialga's roar is pressurizing the air around you! You feel its intensity!")
      # change the battle environment (use animation to transition)
      @sprites["battlebg"].reconfigure(:DIMENSION, :DISTORTION)
      @scene.pbDisplay("Its roar distorted the dimensions!")
      @scene.pbDisplay("Dialga is controlling the domain.")
      # show databoxes
      @scene.pbShowAllDataboxes
    end
  }
  #-----------------------------------------------------------------------------

EliteBattle.add_data(:ORTHRUS, :BACKDROP, { "backdrop" => "Darkness" })
    EliteBattle.add_data(:ORTHRUS, :TRANSITION, "spaceSM")
    EliteBattle.add_data(:ROTOM, :TRANSITION, "spaceSM")
    EliteBattle.add_data(:SAGE1, :BACKDROP, { "backdrop" => "Darkness" },:ACE, 3)

    #---Pokémon Dark Trainers

    AQUA = {
    "turnStart0" => proc do
    @scene.pbTrainerSpeak("You have no chance.")
    end,
    "beforeLastOpp" => proc do
      pbBGMPlay("Battle Elite")
    @scene.pbTrainerSpeak("You have never seen a Pokémon like this before!")
  end,

  "afterLastOpp" => proc do
	@sprites["battlebg"].reconfigure(:DIMENSION, :DISTORTION)
  EliteBattle.playCommonAnimation(:AURAFLARE, @scene, 1)
	@scene.pbDisplay("That Pokémon is drawing power from the Void!")
  @scene.pbDisplay("It's speed greatly increased!")
	@battlers[1].pbRaiseStatStageBasic(:SPEED, 2)
  @scene.pbTrainerSpeak("You can feel the void consuming you...")
  @vector.reset
  EliteBattle.playCommonAnimation(:STATDOWN, @scene, 0)
	@battlers[0].pbReduceHP((@battlers[0].hp/2).floor)
  @scene.pbDisplay("Your HP was reduced!")
	end,
	#"lowHPOpp" => proc do
	#@scene.pbTrainerSpeak("You can feel the void consuming you...")
  #EliteBattle.playCommonAnimation(:STATDOWN, @scene, 0)
	#@battlers[0].pbReduceHP((@battlers[0].hp/2).floor)
  #@scene.pbDisplay("Your HP was reduced!")
	#end,
    "faintedOpp" => "Argh! You will not get any further!",
    "faintedOpp2" => "Fine. Time to show you what Eclipse can do!",
    "attackOpp" => "This is the beginning of your end!",
    "fainted" => "Too easy...",
    "recall" => "That one not good enough?",
    "recall2" => "You must be really worried!",
    "loss" => "Another weakling."
  }

      ROSE = {
    "turnStart0" => proc do
    @scene.pbTrainerSpeak("Welcome to the final lesson! Question!")
                    cmd=0
                    cmd= pbMessage("Where did we first meet?",["Carryport Town","Shellside Town"],1,nil,1)
                    if cmd == 0
                      @scene.pbTrainerSpeak("A day that won't be forgotten!")
                      @battlers[0].pbRaiseStatStageBasic(:ATTACK, 1)
                      EliteBattle.playCommonAnimation(:STATUP, @scene, 0)
                      @scene.pbDisplay("Your Attack was increased!")
                    else
                      @scene.pbTrainerSpeak("You are that forgetful?")
                      @battlers[1].pbRaiseStatStageBasic(:ATTACK, 1)
                      EliteBattle.playCommonAnimation(:STATUP, @scene, 1)
                      @scene.pbDisplay("Your opponents Attack was increased!")
                    end
    end,

    "faintedOpp" => proc do
    @scene.pbTrainerSpeak("Second Question!")
                    cmd=0
                    cmd= pbMessage("How much does a Cheat Sheet cost?",["$1500","$1000"],1,nil,1)
                    if cmd == 0
                      @scene.pbTrainerSpeak("I'm glad you don't know!")
                      @battlers[0].pbRaiseStatStageBasic(:DEFENSE, 1)
                      EliteBattle.playCommonAnimation(:STATUP, @scene, 0)
                      @scene.pbDisplay("Your Defense was increased!")
                    else
                      @scene.pbTrainerSpeak("Ah. I'm ashamed you know that answer.")
                      @battlers[0].pbLowerStatStageBasic(:DEFENSE, 1)
                      EliteBattle.playCommonAnimation(:STATDOWN, @scene, 0)
                      @scene.pbDisplay("Your Defense was lowered!")
                    end
    end,
    "faintedOpp2" => proc do
    @scene.pbTrainerSpeak("Final Question!")
                    cmd=0
                    cmd= pbMessage("What is my final Pokémon?",["Onix","Aggron"],1,nil,1)
                    if cmd == 0
                      @scene.pbTrainerSpeak("How did you get that one?!")
                      @battlers[0].pbRaiseStatStageBasic(:SPEED, 1)
                      EliteBattle.playCommonAnimation(:STATUP, @scene, 0)
                      @scene.pbDisplay("Your Speed was increased!")
                    else
                      @scene.pbTrainerSpeak("Incorrect. You will soon find out what it is!")
                      @battlers[0].pbLowerStatStageBasic(:SPEED, 1)
                      EliteBattle.playCommonAnimation(:STATDOWN, @scene, 0)
                      @scene.pbDisplay("Your Speed was decreased!")
                    end
    end,
    "fainted" => "You should attend some of my classes!",
    "recall" => "Nice switch!",
    "recall2" => "I didn't see this switch coming!",
    "loss" => "Challenge me again when you're ready."
  }

  MICHAELCHARMANDER = {
    "beforeLastOpp" => proc do
    @scene.pbTrainerSpeak("My Tepig will settle this!")
    end,
    "faintedOpp" => "Woah! What an attack!",
    "attackOpp" => "Now this battle can really begin!",
    "fainted" => "I knew that I had this!",
    "recall" => "Switching? A good idea to gain type advantage!",
    "loss" => "Close one!"
  }

    MICHAELBULBASAUR = {
    "beforeLastOpp" => proc do
    @scene.pbTrainerSpeak("My Turtwig will settle this!")
    end,
    "faintedOpp" => "Woah! What an attack!",
    "attackOpp" => "Now this battle can really begin!",
    "fainted" => "I knew that I had this!",
    "recall" => "Switching? A good idea to gain type advantage!",
    "loss" => "Close one!"
  }

     MICHAELSQUIRTLE = {
    "beforeLastOpp" => proc do
    @scene.pbTrainerSpeak("My Froakie will settle this!")
    end,
    "faintedOpp" => "Woah! What an attack!",
    "attackOpp" => "Now this battle can really begin!",
    "fainted" => "I knew that I had this!",
    "recall" => "Switching? A good idea to gain type advantage!",
    "loss" => "Close one!"
  }

    MICHAELCHARMELEON = {
    "beforeLastOpp" => proc do
    @scene.pbTrainerSpeak("Time to see what an evolved Tepig can do!")
    end,
    "faintedOpp" => "Agh. Not again!",
    "fainted" => "I knew that the Gym would strengthen me!",
    "loss" => "Close one! We have improved so much already."
  }

    MICHAELIVYSAUR = {
    "beforeLastOpp" => proc do
    @scene.pbTrainerSpeak("Time to see what an evolved Turtwig can do!")
    end,
    "faintedOpp" => "Agh. Not again!",
    "fainted" => "I knew that the Gym would strengthen me!",
    "loss" => "Close one! We have improved so much already."
  }

     MICHAELWARTORTLE = {
    "beforeLastOpp" => proc do
    @scene.pbTrainerSpeak("Time to see what an evolved Froakie can do!")
    end,
    "faintedOpp" => "Agh. Not again!",
    "fainted" => "I knew that the Gym would strengthen me!",
    "loss" => "Close one! We have improved so much already."
  }

TESTSTRING = {
  "turnStart0" =>  proc do
    echoln @battle.opponent[0].full_name
  end
}

  EliteBattle.add_data(:SAGE1, :BACKDROP, { "backdrop" => "Darkness" },:ACE, 3)
  EliteBattle.add_data(:ECLIPSE, :BACKDROP, { "backdrop" => "Darkness" })
  #EliteBattle.assign_transition("evilTeam", :ECLIPSE, :SAGE1)
  #------Map battlebackgrounds
  EliteBattle.add_data(42, :BACKDROP, {
  "backdrop" => "Forest", "lightsC" => true, "img001" => {
  :bitmap => "forestShade", :z => 1, :flat => true,
  :oy => 0, :y => 94, :sheet => true, :frames => 2, :speed => 16
  }, "trees" => {
    :bitmap => "treePine", :colorize => false, :elements => 8,
    :x => [92,248,300,40,138,216,274,318],
    :y => [132,132,144,118,112,118,110,110],
    :zoom => [1,1,1.1,0.9,0.8,0.85,0.75,0.75],
    :z => [2,2,2,1,1,1,1,1],
  }, "outdoor" => true
})

  EliteBattle.add_data(93, :BACKDROP, {
  "backdrop" => "Forest", "lightsC" => true, "img001" => {
  :bitmap => "forestShade", :z => 1, :flat => true,
  :oy => 0, :y => 94, :sheet => true, :frames => 2, :speed => 16
  }, "trees" => {
    :bitmap => "treePine", :colorize => false, :elements => 8,
    :x => [92,248,300,40,138,216,274,318],
    :y => [132,132,144,118,112,118,110,110],
    :zoom => [1,1,1.1,0.9,0.8,0.85,0.75,0.75],
    :z => [2,2,2,1,1,1,1,1],
  }, "outdoor" => true
})

  EliteBattle.add_data(161, :BACKDROP, {
  "backdrop" => "Forest", "lightsC" => true, "img001" => {
  :bitmap => "forestShade", :z => 1, :flat => true,
  :oy => 0, :y => 94, :sheet => true, :frames => 2, :speed => 16
  }, "trees" => {
    :bitmap => "treePine", :colorize => false, :elements => 8,
    :x => [92,248,300,40,138,216,274,318],
    :y => [132,132,144,118,112,118,110,110],
    :zoom => [1,1,1.1,0.9,0.8,0.85,0.75,0.75],
    :z => [2,2,2,1,1,1,1,1],
  }, "outdoor" => true
})

  EliteBattle.add_data(92, :BACKDROP, {
  "backdrop" => "Forest", "lightsC" => true, "img001" => {
  :bitmap => "forestShade", :z => 1, :flat => true,
  :oy => 0, :y => 94, :sheet => true, :frames => 2, :speed => 16
  }, "trees" => {
    :bitmap => "treePine", :colorize => false, :elements => 8,
    :x => [92,248,300,40,138,216,274,318],
    :y => [132,132,144,118,112,118,110,110],
    :zoom => [1,1,1.1,0.9,0.8,0.85,0.75,0.75],
    :z => [2,2,2,1,1,1,1,1],
  }, "outdoor" => true
})

  EliteBattle.add_data(95, :BACKDROP, {
  "backdrop" => "Forest", "lightsC" => true, "img001" => {
  :bitmap => "forestShade", :z => 1, :flat => true,
  :oy => 0, :y => 94, :sheet => true, :frames => 2, :speed => 16
  }, "trees" => {
    :bitmap => "treePine", :colorize => false, :elements => 8,
    :x => [92,248,300,40,138,216,274,318],
    :y => [132,132,144,118,112,118,110,110],
    :zoom => [1,1,1.1,0.9,0.8,0.85,0.75,0.75],
    :z => [2,2,2,1,1,1,1,1],
  }, "outdoor" => true
})

  EliteBattle.add_data(158, :BACKDROP, {
  "backdrop" => "Forest", "lightsC" => true, "img001" => {
  :bitmap => "forestShade", :z => 1, :flat => true,
  :oy => 0, :y => 94, :sheet => true, :frames => 2, :speed => 16
  }, "trees" => {
    :bitmap => "treePine", :colorize => false, :elements => 8,
    :x => [92,248,300,40,138,216,274,318],
    :y => [132,132,144,118,112,118,110,110],
    :zoom => [1,1,1.1,0.9,0.8,0.85,0.75,0.75],
    :z => [2,2,2,1,1,1,1,1],
  }, "outdoor" => true
})


  EliteBattle.add_data(150, :BACKDROP, {
  "backdrop" => "Forest", "lightsC" => true, "img001" => {
  :bitmap => "forestShade", :z => 1, :flat => true,
  :oy => 0, :y => 94, :sheet => true, :frames => 2, :speed => 16
  }, "trees" => {
    :bitmap => "treePine", :colorize => false, :elements => 8,
    :x => [92,248,300,40,138,216,274,318],
    :y => [132,132,144,118,112,118,110,110],
    :zoom => [1,1,1.1,0.9,0.8,0.85,0.75,0.75],
    :z => [2,2,2,1,1,1,1,1],
  }, "outdoor" => true
})


  EliteBattle.add_data(87, :BACKDROP, {
  "backdrop" => "City", "outdoor" => true
})

  EliteBattle.add_data(43, :BACKDROP, {
  "backdrop" => "City", "outdoor" => true
})

  EliteBattle.add_data(81, :BACKDROP, {
  "backdrop" => "Darkness", "outdoor" => true
})


  EliteBattle.add_data(78, :BACKDROP, {
  "backdrop" => "IndoorA", "outdoor" => false
})

  EliteBattle.add_data(120, :BACKDROP, {
  "backdrop" => "IndoorA", "outdoor" => false
})

  EliteBattle.add_data(121, :BACKDROP, {
  "backdrop" => "IndoorA", "outdoor" => false
})

  EliteBattle.add_data(110, :BACKDROP, {
  "backdrop" => "Cave", "outdoor" => false
})
  EliteBattle.add_data(112, :BACKDROP, {
  "backdrop" => "Cave", "outdoor" => false
})
  EliteBattle.add_data(111, :BACKDROP, {
  "backdrop" => "Cave", "outdoor" => false
})
  EliteBattle.add_data(113, :BACKDROP, {
  "backdrop" => "CaveDark", "outdoor" => false
})
  EliteBattle.add_data(126, :BACKDROP, {
  "backdrop" => "CaveDark", "outdoor" => false
})
  EliteBattle.add_data(135, :BACKDROP, {
  "backdrop" => "CaveDark", "outdoor" => false
})

  EliteBattle.add_data(144, :BACKDROP, {
  "backdrop" => "CaveDark", "outdoor" => false
})

  EliteBattle.add_data(145, :BACKDROP, {
  "backdrop" => "CaveDark", "outdoor" => false
})

  EliteBattle.add_data(137, :BACKDROP, {
  "backdrop" => "CaveDark", "outdoor" => false
})

  EliteBattle.add_data(91, :BACKDROP, {
  "backdrop" => "IndoorA", "outdoor" => false
})

#-----Online
EliteBattle.assign_bgm("online.mp3", :POKEMONTRAINER_PLAYERM, :POKEMONTRAINER_PLAYERF)
EliteBattle.add_data(:POKEMONTRAINER_PLAYERM, :BACKDROP, {    "backdrop" => "Net", "img001" => {
      :scrolling => true, :vertical => true, :speed => 1,
      :bitmap => "decor003d",
      :oy => 180, :y => 90, :flat => true
    }, "img002" => {
      :bitmap => "crowd_d",
      :oy => 32, :y => 112, :z => 2, :flat => false, :sheet => true,
      :vertical => true, :speed => 8, :frames => 2
    }})
    EliteBattle.add_data(:POKEMONTRAINER_PLAYERF, :BACKDROP, {    "backdrop" => "Net", "img001" => {
      :scrolling => true, :vertical => true, :speed => 1,
      :bitmap => "decor003d",
      :oy => 180, :y => 90, :flat => true
    }, "img002" => {
      :bitmap => "crowd_d",
      :oy => 32, :y => 112, :z => 2, :flat => false, :sheet => true,
      :vertical => true, :speed => 8, :frames => 2
    }})
#EliteBattle.add_data(:POKEMONTRAINER_PLAYERF, :BACKDROP, {})
#EliteBattle.add_data(:POKEMONTRAINER_PLAYERM, :BACKDROP, { "backdrop" => "Online", "lightsA" => true})
#EliteBattle.add_data(:POKEMONTRAINER_PLAYERF, :BACKDROP, { "backdrop" => "Online", "lightsA" => true})
EliteBattle.assign_transition("specialSM", :POKEMONTRAINER_PLAYERM, :POKEMONTRAINER_PLAYERF)

end
